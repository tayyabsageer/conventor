Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root "welcome#index"
  get "/png_to_jpeg", to: "image#png_to_jpeg"
  post "/jpeg_conventor", to: "image#jpeg_conventor", as: "jpeg_convertor"
end
