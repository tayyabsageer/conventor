class ProductSubCategory < ApplicationRecord
  belongs_to :product_category
  has_many :products, dependent: :destroy
  has_many :sales,dependent: :destroy
end
