class Product < ApplicationRecord
  belongs_to :product_sub_category
  belongs_to :product_category
  has_many :product_variations, dependent: :destroy
  has_many :sales, dependent: :destroy
  has_many :stocks, dependent: :destroy
end
