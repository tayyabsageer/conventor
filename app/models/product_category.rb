class ProductCategory < ApplicationRecord
  has_many :product_sub_categories, dependent: :destroy
  has_many :products, dependent: :destroy
  has_many :stocks,dependent: :destroy
end
