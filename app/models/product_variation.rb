class ProductVariation < ApplicationRecord
  belongs_to :product
  has_many :variation_sizes, dependent: :destroy
end
