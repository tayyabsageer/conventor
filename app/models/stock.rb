class Stock < ApplicationRecord
  belongs_to :product
  belongs_to :product_sub_category
  belongs_to :product_category
end
