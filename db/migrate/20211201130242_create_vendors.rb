class CreateVendors < ActiveRecord::Migration[6.1]
  def change
    create_table :vendors do |t|
      t.string :name
      t.string :phonenumber
      t.text :address

      t.timestamps
    end
  end
end
