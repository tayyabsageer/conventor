class CreateProductSubCategories < ActiveRecord::Migration[6.1]
  def change
    create_table :product_sub_categories do |t|
      t.string :name
      t.references :product, null: false, foreign_key: true

      t.timestamps
    end
  end
end
