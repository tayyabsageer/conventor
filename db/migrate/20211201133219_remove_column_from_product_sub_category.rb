class RemoveColumnFromProductSubCategory < ActiveRecord::Migration[6.1]
  def change
    remove_reference :product_sub_categories, :product, null: false, foreign_key: true
  end
end
