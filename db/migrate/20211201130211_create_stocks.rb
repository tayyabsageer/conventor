class CreateStocks < ActiveRecord::Migration[6.1]
  def change
    create_table :stocks do |t|
      t.datetime :entry_date
      t.references :product, null: false, foreign_key: true
      t.references :product_sub_category, null: false, foreign_key: true
      t.references :product_category, null: false, foreign_key: true
      t.integer :number_of_pair

      t.timestamps
    end
  end
end
