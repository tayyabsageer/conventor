class CreateVariationSizes < ActiveRecord::Migration[6.1]
  def change
    create_table :variation_sizes do |t|
      t.string :colour
      t.string :style
      t.references :product_variation, null: false, foreign_key: true

      t.timestamps
    end
  end
end
