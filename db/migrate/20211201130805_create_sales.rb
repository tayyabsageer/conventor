class CreateSales < ActiveRecord::Migration[6.1]
  def change
    create_table :sales do |t|
      t.references :product, null: false, foreign_key: true
      t.float :total_amount_paid
      t.float :return_amount

      t.timestamps
    end
  end
end
