class AddVendorToStock < ActiveRecord::Migration[6.1]
  def change
    add_reference :stocks, :vendor, null: false, foreign_key: true
  end
end
