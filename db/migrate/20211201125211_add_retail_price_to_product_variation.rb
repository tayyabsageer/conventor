class AddRetailPriceToProductVariation < ActiveRecord::Migration[6.1]
  def change
    add_column :product_variations, :discount, :float
    add_column :product_variations, :sale_price, :float
    add_column :product_variations, :retail_price, :float
  end
end
