class AddBillToStock < ActiveRecord::Migration[6.1]
  def change
    add_column :stocks, :bill_paid, :float
  end
end
