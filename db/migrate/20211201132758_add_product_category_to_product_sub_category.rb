class AddProductCategoryToProductSubCategory < ActiveRecord::Migration[6.1]
  def change
    add_reference :product_sub_categories, :product_category, null: false, foreign_key: true
  end
end
