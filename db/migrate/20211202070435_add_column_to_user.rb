class AddColumnToUser < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :address, :text
    add_column :users, :phonenumber, :string
    add_column :users, :date_of_birth, :datetime
  end
end
