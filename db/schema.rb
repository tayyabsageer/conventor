# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_12_02_070435) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "models", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "User"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_models_on_email", unique: true
    t.index ["reset_password_token"], name: "index_models_on_reset_password_token", unique: true
  end

  create_table "product_categories", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "product_sub_categories", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "product_category_id", null: false
    t.index ["product_category_id"], name: "index_product_sub_categories_on_product_category_id"
  end

  create_table "product_variations", force: :cascade do |t|
    t.string "size"
    t.float "price"
    t.bigint "product_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "discount"
    t.float "sale_price"
    t.float "retail_price"
    t.index ["product_id"], name: "index_product_variations_on_product_id"
  end

  create_table "products", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "product_sub_category_id", null: false
    t.bigint "product_category_id", null: false
    t.index ["product_category_id"], name: "index_products_on_product_category_id"
    t.index ["product_sub_category_id"], name: "index_products_on_product_sub_category_id"
  end

  create_table "sales", force: :cascade do |t|
    t.bigint "product_id", null: false
    t.float "total_amount_paid"
    t.float "return_amount"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["product_id"], name: "index_sales_on_product_id"
  end

  create_table "stocks", force: :cascade do |t|
    t.datetime "entry_date"
    t.bigint "product_id", null: false
    t.bigint "product_sub_category_id", null: false
    t.bigint "product_category_id", null: false
    t.integer "number_of_pair"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "vendor_id", null: false
    t.float "bill_paid"
    t.index ["product_category_id"], name: "index_stocks_on_product_category_id"
    t.index ["product_id"], name: "index_stocks_on_product_id"
    t.index ["product_sub_category_id"], name: "index_stocks_on_product_sub_category_id"
    t.index ["vendor_id"], name: "index_stocks_on_vendor_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email"
    t.string "password_digest"
    t.string "first_name"
    t.string "last_name"
    t.string "remember_token"
    t.datetime "remember_token_expires_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.text "address"
    t.string "phonenumber"
    t.datetime "date_of_birth"
  end

  create_table "variation_sizes", force: :cascade do |t|
    t.string "colour"
    t.string "style"
    t.bigint "product_variation_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["product_variation_id"], name: "index_variation_sizes_on_product_variation_id"
  end

  create_table "vendors", force: :cascade do |t|
    t.string "name"
    t.string "phonenumber"
    t.text "address"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "product_sub_categories", "product_categories"
  add_foreign_key "product_variations", "products"
  add_foreign_key "products", "product_categories"
  add_foreign_key "products", "product_sub_categories"
  add_foreign_key "sales", "products"
  add_foreign_key "stocks", "product_categories"
  add_foreign_key "stocks", "product_sub_categories"
  add_foreign_key "stocks", "products"
  add_foreign_key "stocks", "vendors"
  add_foreign_key "variation_sizes", "product_variations"
end
